package edu.kovalyshyn.model.task_array;

import java.util.Arrays;

public class Main {

  public static void main(String[] args) {
    LogicalTask task = new LogicalTask();
    System.out.println("-------Task A--------");
    System.out.println("generate char arrays:");
    task.generateTwoArray();
    System.out.println("the third array with elements are present at both arrays:");
    System.out.println(Arrays.toString(task.presentAtTwoArray()));
    System.out.println("the third array with elements are present at first array:");
    System.out.println(Arrays.toString(task.presentAtOneArray()));
    System.out.println("\n-------Task B the same as Task C--------");
    System.out.println("Only the unique element in the first array :");
    System.out.println(Arrays.toString(
        task.getListOne().stream().distinct().toArray()));

  }

}
