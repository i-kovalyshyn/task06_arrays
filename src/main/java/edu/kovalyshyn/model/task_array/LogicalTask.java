package edu.kovalyshyn.model.task_array;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import lombok.Data;

@Data
public class LogicalTask {

  private int amount;
  private List<Character> listOne = new ArrayList<>();
  private List<Character> listTwo = new ArrayList<>();

  public LogicalTask() {
    Random rand = new Random();
    amount = (int) (20 * rand.nextDouble());
  }

  private char[] randomCharArray(int len) {
    StringBuilder b = new StringBuilder();
    for (int i = 0; i < len; i++) {
      Random rnd = new Random();
      char c = (char) (rnd.nextInt(26) + 'a');
      b.append(c);
    }
    return b.toString().toCharArray();
  }

  void generateTwoArray() {

    char[] charArrayOne = randomCharArray(amount);
    System.out.println(Arrays.toString(charArrayOne));
    for (char c : charArrayOne) {
      listOne.add(c);
    }

    char[] charArrayTwo = randomCharArray(amount);
    System.out.println(Arrays.toString(charArrayTwo));
    for (char c : charArrayTwo) {
      listTwo.add(c);
    }
  }

  Object[] presentAtTwoArray() {
    return listOne.stream()
        .distinct()
        .filter(x -> listTwo.stream().anyMatch(y -> y == x)).toArray();
  }

  Object[] presentAtOneArray() {
    return listOne.stream()
        .distinct()
        .filter(x -> listTwo.stream().noneMatch(y -> y == x)).toArray();
  }


}
