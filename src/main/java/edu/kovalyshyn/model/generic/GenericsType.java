package edu.kovalyshyn.model.generic;

public class GenericsType<T> {

  private T obj;


  public T get() {
    return this.obj;
  }

  public void set(T t) {
    this.obj = t;
  }
}
