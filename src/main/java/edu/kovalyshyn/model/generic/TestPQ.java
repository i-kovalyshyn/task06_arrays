package edu.kovalyshyn.model.generic;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TestPQ implements Comparable<TestPQ> {

  private String string;
  private static Logger log = LogManager.getLogger(TestPQ.class);

  private TestPQ(String string) {
    this.string = string;
  }

  public TestPQ() {

  }

  @Override
  public int compareTo(TestPQ other) {

    return Integer.compare(this.string.compareTo(other.string), 0);
  }

  @Override
  public String toString() {
    return "TestPQ's string_sort " + string;
  }

  public void testPQ() {
    PriorityQueue<TestPQ> queue = new PriorityQueue<>();

    queue.add(new TestPQ("D"));
    queue.add(new TestPQ("F"));
    queue.add(new TestPQ("C"));
    queue.add(new TestPQ("A"));
    queue.add(new TestPQ("B"));

    log.info("Printing out queue:");
    queue.priorityCompare();
    queue.print();
    log.info("--------------------------------------------");

    log.info("Priority Search:");
    log.info(queue.prioritySearch());
    log.info("--------------------------------------------");

    log.info("Removing: ");
    log.info("This item was removed: " + queue.remove());
    log.info("This item was removed: " + queue.remove());
    log.info("This item was removed: " + queue.remove());
    log.info("--------------------------------------------");

    log.info("Priority Search:");
    log.info(queue.prioritySearch());
    log.info("--------------------------------------------");
    log.info("BUILD SUCCESSFUL");

  }

}
