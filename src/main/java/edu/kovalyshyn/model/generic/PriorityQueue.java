package edu.kovalyshyn.model.generic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class PriorityQueue<T extends Comparable<? super T>>  {

  private List<T> arrayList = new ArrayList<T>();

  void add(T obj) {
    arrayList.add(obj);
  }

  T prioritySearch() {
    T priority = arrayList.get(0);
    for (T t : arrayList) {
      if (t.compareTo(priority) > 0) {
        priority = t;
      }
    }
    return priority;
  }

  void priorityCompare() {
    int out, in;
    for (out = arrayList.size() - 1; out > 0; out--) {
      for (in = 0; in < out; in++) {
        if (arrayList.get(in).compareTo(arrayList.get(in + 1)) > 0) {
          swap(in, in + 1);
        }
      }
    }
  }

  private void swap(int one, int two) {
    Collections.swap(arrayList, one, two);
  }

  Object remove() {
    if (arrayList.isEmpty()) {
      return null;
    }
    T obj = prioritySearch();
    arrayList.remove(obj);
    return obj;
  }

  void  print(){
    for (T t : arrayList) {
      System.out.println(t);
    }
  }


}
