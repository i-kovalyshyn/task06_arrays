package edu.kovalyshyn.model.string_sort;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class CityArray {

  public void cityArraySort() {

    List<DaoCity> cityLinst = new ArrayList<>(Arrays.asList(
        new DaoCity('A', "Lviv"),
        new DaoCity('F', "Kharkiv"),
        new DaoCity('H', "Dnipro"),
        new DaoCity('B', "Odessa"),
        new DaoCity('D', "Mykolaiv")
    ));
    System.out.println("City List:");
    System.out.println(cityLinst);
    System.out.println("Sorted by implements Comparable in ASC by char:");
    Collections.sort(cityLinst);
    System.out.println(cityLinst);

    Comparator<DaoCity> daoCityCompator =
        (a, b) -> a.getCity().compareTo(b.getCity()) >=
            b.getCity().compareTo(a.getCity()) ? 1 : -1;

    cityLinst.sort(daoCityCompator);

    System.out.println("Sorted by oun comparator in ASC by city:");
    System.out.println(cityLinst);

    int index;
    index = Collections.binarySearch(cityLinst, new DaoCity('A', "Lviv"), daoCityCompator);
    System.out.println("index of Lviv " + index);
  }

}
