package edu.kovalyshyn.model.string_sort;

import lombok.Data;

@Data
public class DaoCity implements Comparable<DaoCity>{

  private Character chars;
  private String city;

  public DaoCity(Character chars, String city) {
    this.chars = chars;
    this.city = city;
  }

  @Override
  public String toString() {
    return "{" +
         ""+ chars +
        ", " + city + '\'' +
        '}';
  }

  @Override
  public int compareTo(DaoCity daoCity) {
    //ASC
    return chars.compareTo(daoCity.chars);
  }
}
