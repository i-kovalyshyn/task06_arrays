package edu.kovalyshyn.model.deque;

/**
 * implementation of De-queue using circular array
 */
class Deque {

  static final int MAX = 100;
  int[] array;
  int front;
  int rear;
  int size;

  public Deque(int size) {
    array = new int[MAX];
    front = -1;
    rear = 0;
    this.size = size;
  }

  // Checks whether Deque is full or not.
  boolean isFull() {
    return ((front == 0 && rear == size - 1) || front == rear + 1);
  }

  // Checks whether Deque is empty or not.
  boolean isEmpty() {
    return (front == -1);
  }

  // Inserts an element at front
  void insertFront(int key) {
    if (isFull()) {
      System.out.println("Overflow");
      return;
    }
    if (front == -1) {
      front = 0;
      rear = 0;
    } else if (front == 0) {
      front = size - 1;
    } else {
      front = front - 1;
    }
    array[front] = key;
  }

  // to inset element at rear end Deque
  void insertRear(int key) {
    if (isFull()) {
      System.out.println(" Overflow ");
      return;
    }
    // If queue is initially empty
    if (front == -1) {
      front = 0;
      rear = 0;
    } else if (rear == size - 1) {
      rear = 0;
    } else {
      rear = rear + 1;
    }
    array[rear] = key;
  }

  // Deletes element at front end of Deque
  void deleteFront() {
    if (isEmpty()) {
      System.out.println("Queue Underflow\n");
      return;
    }
    if (front == rear) {
      front = -1;
      rear = -1;
    } else if (front == size - 1) {
      front = 0;
    } else {
      front = front + 1;
    }
  }

  // Delete element at rear end of Deque
  void deleteRear() {
    if (isEmpty()) {
      System.out.println(" Underflow");
      return;
    }
    if (front == rear) {
      front = -1;
      rear = -1;
    } else if (rear == 0) {
      rear = size - 1;
    } else {
      rear = rear - 1;
    }
  }

  // Returns front element of Deque
  int getFront() {
    if (isEmpty()) {
      System.out.println(" Underflow");
      return -1;
    }
    return array[front];
  }

  // function return rear element of Deque
  int getRear() {
    if (isEmpty() || rear < 0) {
      System.out.println(" Underflow\n");
      return -1;
    }
    return array[rear];
  }
}



