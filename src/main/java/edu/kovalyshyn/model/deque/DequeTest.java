package edu.kovalyshyn.model.deque;

public class DequeTest {

  public void dequeTest() {
    Deque deque = new Deque(5);
    System.out.println("Insert element at rear end  : 5 ");
    deque.insertRear(5);
    System.out.println("insert element at rear end : 10 ");
    deque.insertRear(10);
    System.out.println("get rear element : " + deque.getRear());
    deque.deleteRear();
    System.out.println("After delete rear element new rear become : " + deque.getRear());
    System.out.println("inserting element at front end");
    deque.insertFront(15);
    System.out.println("get front element: " + deque.getFront());
    deque.deleteFront();
    System.out.println("After delete front element new front become : "+deque.getFront());

  }
}
