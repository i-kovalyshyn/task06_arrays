package edu.kovalyshyn.model.string_array;

import java.util.Arrays;
import lombok.Data;

@Data
public class StringContainer {

  private String[] stringsArray;

  StringContainer() {
    stringsArray = new String[1];
  }

  public void add(String string) {
    if (stringsArray[0] != null) {
      String[] temp = Arrays.copyOf(stringsArray, stringsArray.length + 1);
      temp[stringsArray.length] = string;
      stringsArray = temp;
    } else {
      stringsArray[0] = string;
    }
  }
  public String get (int index){
    if(index>stringsArray.length){
      return "";
    }
    return stringsArray[index];
  }
  public int size(){
    return stringsArray.length;
  }
}
