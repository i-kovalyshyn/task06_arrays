package edu.kovalyshyn.model.string_array;

import java.util.Arrays;

public class Main {

  public static void main(String[] args) {
    StringContainer stringContainer = new StringContainer();
    stringContainer.add("A");
    System.out.println(Arrays.toString(stringContainer.getStringsArray()));
    stringContainer.add("B");
    System.out.println(Arrays.toString(stringContainer.getStringsArray()));
    stringContainer.add("C");
    System.out.println(Arrays.toString(stringContainer.getStringsArray()));
    stringContainer.add("D");
    System.out.println(Arrays.toString(stringContainer.getStringsArray()));
  }

}
