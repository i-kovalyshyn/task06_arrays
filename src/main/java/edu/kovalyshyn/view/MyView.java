package edu.kovalyshyn.view;

import edu.kovalyshyn.controller.Controller;
import edu.kovalyshyn.controller.ControllerImpl;
import edu.kovalyshyn.model.deque.DequeTest;
import edu.kovalyshyn.model.generic.GenericsType;
import edu.kovalyshyn.model.generic.TestPQ;
import edu.kovalyshyn.model.string_sort.CityArray;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MyView {

  private Controller controller;
  private Map<String, String> menu;
  private Map<String, Printable> methodsMenu;
  private static Scanner input = new Scanner(System.in);
  private static Logger log = LogManager.getLogger(MyView.class);


  public MyView() {
    controller = new ControllerImpl();
    menu = new LinkedHashMap<>();
    menu.put("1", " 1 print generic");
    menu.put("2", " 2 Test my oun Priority Queue");
    menu.put("3", " 3 Test  Deque");
    menu.put("4", " 4 use Comparable and Comparator");
    menu.put("Q", " Q - EXIT");

    methodsMenu = new LinkedHashMap<>();
    methodsMenu.put("1", this::pressButton1);
    methodsMenu.put("2", this::pressButton2);
    methodsMenu.put("3", this::pressButton3);
    methodsMenu.put("4", this::pressButton4);

  }

  private void pressButton1() {
    GenericsType obj = new GenericsType();
    Scanner scanner = new Scanner(System.in);

    System.out.println("Please type something:");
    obj.set(scanner.next());
    log.info("Your input is:" + obj.get());
  }

  private void pressButton2() {
    TestPQ testPQ = new TestPQ();
    testPQ.testPQ();
  }

  private void pressButton3() {
    DequeTest dequeTest=new DequeTest();
    dequeTest.dequeTest();
  }
  private void pressButton4() {
    CityArray cityArray = new CityArray();
    cityArray.cityArraySort();
  }



  private void outputMenu() {
    System.out.println("\n MENU:");
    for (String str : menu.values()) {
      System.out.println(str);
    }
  }

  public void show() {
    String keyMenu;
    do {
      outputMenu();
      System.out.println("Please, select menu point.");
      keyMenu = input.nextLine().toUpperCase();
      try {
        methodsMenu.get(keyMenu).print();
      } catch (Exception e) {
        log.info("-EXIT- ");
      }
    } while (!keyMenu.equals("Q"));
  }
}
